\documentclass{article}

\usepackage{amsmath, amsfonts, amssymb, mathrsfs, url}

\title{VerTeX User's Manual}
\author{Steve Kieffer\footnote{{\tt http://skieffer.info}}}
%\email{\url{http://skieffer.info}}
\date{Updated: 18 January 2013}

\begin{document}
\maketitle
\tableofcontents

\section{Introduction}
{\tt VerTeX} is a pre-filter designed to make typing TeX / LaTeX files
easier. For the remainder of this manual, ``TeX'' will mean TeX and/or
LaTeX.

The name {\tt VerTeX} stands for ``Verbal TeX'', since the language
allows you to write TeX in a way that is closer to what you say when
you read mathematics aloud (if you ever do that), and because it offers many
verbal alternatives to symbolic notations, allowing you to keep
your fingers over the home row on the keyboard.
The word {\tt VerTeX} is pronounced ``ver-tech''.

{\tt VerTeX} is a Python script, and if you have the latest (or not
even the latest)
Python 2.x it should run just fine.
I have not tested it on any Python 3.x.

The features of {\tt VerTeX} are:
\begin{enumerate}
  \item Semiautomatic subscripts and superscripts
    \begin{center}
      Example: {\tt a1, a2, ddd, an} produces $a_{1},a_{2},\ldots,a_{n}$.
    \end{center}
  \item Few to no backslashes in math mode
    \begin{center}
      Example: {\tt alpha mapsto beta} produces $\alpha\mapsto\beta$.
    \end{center}
  \item Font prefixes
    \begin{center}
      Example: {\tt frp in bbZ} produces $\mathfrak{p}\in\mathbb{Z}$.
    \end{center}
  \item Keyword delimiting patterns instead of braces
    \begin{center}
      Example: {\tt frac x over y;} produces $\frac{x}{y}$.
    \end{center}
  \item Macro definitions on the fly
  \item Transparency to ordinary TeX
\end{enumerate}

In particular, transparency to ordinary TeX means that, with only a
couple of exceptions (to be covered in Sections
\ref{sec:backslashes} and \ref{sec:semiautomatic} below), a {\tt
  VerTeX} file could potentially contain nothing but ordinary TeX. You
are free to use as many or as few of {\tt VerTeX}'s features as you
wish.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\medskip
\noindent {\bf Installing and Using {\tt VerTeX}:}

\medskip
Download the Python script {\tt vertex} and save it anywhere on your
{\tt PATH}.

You write a {\tt VerTeX} file exactly as you would write a {\tt TeX} (or
{\tt LaTeX}) file, except that within math mode
\footnote{I.e.~any math mode: inline, display, equation environment,
  what have you.}
you are free to use the {\tt VerTeX} syntax.

You should give your {\tt VerTeX} file the extension {\tt .vtx}.
After it passes through the {\tt VerTeX} filter, a file with extension
{\tt .tex} will be automatically produced.

You may use the {\tt \textbackslash{}input} command to build large
documents exactly as you would in ordinary {\tt LaTeX}. Just as you
would do there, do \emph{not} put the file extension on the end of the
name of the file passed to the {\tt \textbackslash{}input}
command. However, the file itself should have the {\tt .vtx}
extension.

At the command line, type
\begin{center}
  {\tt vertex MODE FILENAME}
\end{center}
where {\tt MODE} is one of the switches {\tt -t}, {\tt -l}, or {\tt -p}, and
{\tt FILENAME} is the name of the {\tt .vtx} file to be processed. Note that
the {\tt MODE} cannot be omitted. You must choose one of the three.

\begin{itemize}
\item {\tt MODE} {\tt -t}: In this mode {\tt vertex} merely translates your {\tt
  .vtx} file into a {\tt .tex} file.

\item {\tt MODE} {\tt -l} and {\tt -p}: The translation is performed as in the
{\tt -t} mode, and then the output {\tt .tex} file is passed on,
either to {\tt latex} (in {\tt -l} mode), or to {\tt pdflatex} (in
{\tt -p} mode).
\end{itemize}

We now proceed to discuss the features of VerTeX.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Slash the Backslashes}
\label{sec:backslashes}

When you are writing mathematics, how often do you want an `$\alpha$',
and how often do you want to multiply the variables $a$, $l$, $p$,
$h$, and $a$ together, in that order? Why then should {\tt\$alpha\$}
give you a sequence of Roman letters, while the Greek letter requires
a backslash?

In {\tt VerTeX}, {\tt \$alpha\$} yields `$\alpha$', and if you really
want the product of variables, simply put spaces between the letters,
as in {\tt \$a l p h a\$}.

\bigskip
In general, {\tt VerTeX} keywords are not strings of letters preceded
by a backslash, but simply strings of letters uninterrupted by
whitespace.

Conceptually, the keywords in {\tt VerTeX} may be divided into the
following four kinds, according to the role they play in avoiding
backslashes. 

\medskip
\noindent Types of VerTeX keywords:
\begin{enumerate}
  \item bsme
  \item built-in
  \item bracket word
  \item font prefix
\end{enumerate}

\subsection{bsmes}

The abbreviation `bsme' is short for ``backslash me'', and a bsme
keyword is one that is exactly the same as a keyword in {\tt TeX},
except without the backslash. It produces the exact same result as the
corresponding {\tt TeX} keyword. Table 1 shows all
the bsme keywords in {\tt VerTeX}. Each one produces the same effect in
{\tt VerTeX} as if you had put a backslash in front of it and typed it
in {\tt TeX}.  So for example, {\tt \$subseteq\$} achieves the same
thing in {\tt VerTeX} as {\tt \$\textbackslash{}subseteq\$} does in
{\tt TeX}.

You will notice that some of the Greek letters (namely,
those whose names are two-letters long) are missing from this
list. This is intentional, and is discussed in Section
\ref{subsec:builtins}.

%\begin{table}[h]
%\begin{center}
%  \begin{tabular}{|c|c|c|c|c|c|} \hline
%{\tt mapsto} & $mapsto$ & {\tt leq} & $leq$ & {\tt geq} & $geq$ \\\hline
%{\tt neq} & $neq$ & {\tt times} & $times$ & {\tt subseteq} & $subseteq$ \\\hline
%{\tt supseteq} & $supseteq$ & {\tt subsetneq} & $subsetneq$ & {\tt supsetneq} & $supsetneq$ \\\hline
%{\tt subset} & $subset$ & {\tt supset} & $supset$ & {\tt not} & $not$ \\\hline
%{\tt cdot} & $cdot$ & {\tt det} & $det$ & {\tt equiv} & $equiv$ \\\hline
%{\tt cong} & $cong$ & {\tt sim} & $sim$ & {\tt cup} & $cup$ \\\hline
%{\tt cap} & $cap$ & {\tt nmid} & $nmid$ & {\tt mid} & $mid$ \\\hline
%{\tt infty} & $infty$ & {\tt ker} & $ker$ & {\tt iff} & $iff$ \\\hline
%{\tt forall} & $forall$ & {\tt exists} & $exists$ & {\tt int} & $int$ \\\hline
%{\tt sin} & $sin$ & {\tt cos} & $cos$ & {\tt log} & $log$ \\\hline
%{\tt exp} & $exp$ & {\tt quad} & $quad$ & {\tt qquad} & $qquad$ \\\hline
%{\tt alpha} & $alpha$ & {\tt beta} & $beta$ & {\tt gamma} & $gamma$ \\\hline
%{\tt delta} & $delta$ & {\tt epsilon} & $epsilon$ & {\tt zeta} & $zeta$ \\\hline
%{\tt eta} & $eta$ & {\tt theta} & $theta$ & {\tt iota} & $iota$ \\\hline
%{\tt kappa} & $kappa$ & {\tt lambda} & $lambda$ & {\tt rho} & $rho$ \\\hline
%{\tt sigma} & $sigma$ & {\tt tau} & $tau$ & {\tt upsilon} & $upsilon$ \\\hline
%{\tt phi} & $phi$ & {\tt chi} & $chi$ & {\tt psi} & $psi$ \\\hline
%{\tt omega} & $omega$ & {\tt Gamma} & $Gamma$ & {\tt Delta} & $Delta$ \\\hline
%{\tt Theta} & $Theta$ & {\tt Lambda} & $Lambda$ & {\tt Sigma} & $Sigma$ \\\hline
%{\tt Phi} & $Phi$ & {\tt Psi} & $Psi$ & {\tt Omega} & $Omega$ \\\hline
%{\tt ell} & $ell$
%  \end{tabular}
%  \label{list:bsmes}
%\end{center}
%\end{table}

\begin{table}[h]
\begin{center}
  \begin{tabular}{|c|c|c|c|c|c|c|c|} \hline
{\tt mapsto} & $\mapsto$ & {\tt leq} & $\leq$ & {\tt geq} & $\geq$ & {\tt neq} & $\neq$ \\\hline
{\tt times} & $\times$ & {\tt subseteq} & $\subseteq$ & {\tt supseteq} & $\supseteq$ & {\tt subsetneq} & $\subsetneq$ \\\hline
{\tt supsetneq} & $\supsetneq$ & {\tt subset} & $\subset$ & {\tt supset} & $\supset$ & {\tt not} & $\not$ \\\hline
{\tt cdot} & $\cdot$ & {\tt det} & $\det$ & {\tt equiv} & $\equiv$ & {\tt cong} & $\cong$ \\\hline
{\tt sim} & $\sim$ & {\tt cup} & $\cup$ & {\tt cap} & $\cap$ & {\tt nmid} & $\nmid$ \\\hline
{\tt mid} & $\mid$ & {\tt infty} & $\infty$ & {\tt ker} & $\ker$ & {\tt iff} & $\iff$ \\\hline
{\tt forall} & $\forall$ & {\tt exists} & $\exists$ & {\tt int} & $\int$ & {\tt sin} & $\sin$ \\\hline
{\tt cos} & $\cos$ & {\tt log} & $\log$ & {\tt exp} & $\exp$ & {\tt quad} & $\quad$ \\\hline
{\tt qquad} & $\qquad$ & {\tt alpha} & $\alpha$ & {\tt beta} & $\beta$ & {\tt gamma} & $\gamma$ \\\hline
{\tt delta} & $\delta$ & {\tt epsilon} & $\epsilon$ & {\tt zeta} & $\zeta$ & {\tt eta} & $\eta$ \\\hline
{\tt theta} & $\theta$ & {\tt iota} & $\iota$ & {\tt kappa} & $\kappa$ & {\tt lambda} & $\lambda$ \\\hline
{\tt rho} & $\rho$ & {\tt sigma} & $\sigma$ & {\tt tau} & $\tau$ & {\tt upsilon} & $\upsilon$ \\\hline
{\tt phi} & $\phi$ & {\tt chi} & $\chi$ & {\tt psi} & $\psi$ & {\tt omega} & $\omega$ \\\hline
{\tt Gamma} & $\Gamma$ & {\tt Delta} & $\Delta$ & {\tt Theta} & $\Theta$ & {\tt Lambda} & $\Lambda$ \\\hline
{\tt Sigma} & $\Sigma$ & {\tt Phi} & $\Phi$ & {\tt Psi} & $\Psi$ & {\tt Omega} & $\Omega$ \\\hline
{\tt ell} & $\ell$ &&&&&&\\\hline
  \end{tabular}
  \label{list:bsmes}
\end{center}
\caption{bsme keywords}
\end{table}

\subsection{built-ins}
\label{subsec:builtins}

The so-called ``built-in'' keywords do not correspond to any existing
{\tt TeX} keywords. They do not take any arguments, but simply
translate directly into some string of {\tt TeX}, and their purpose is
to in one way or another give an easier way to type certain commonly
used {\tt TeX} strings.

In particular, one of the goals of {\tt VerTeX} is to give you the
option to keep your fingers over the home row while typing, and for
this reason the built-in keywords provide many alphabetical
equivalents to {\tt TeX} strings that ordinarily involve
non-alphabetical characters. For example, {\tt inv} (for ``inverse'')
produces {\tt \textasciicircum{}\{-1\}}, and {\tt squ} (for ``squared'')
produces {\tt \textasciicircum{}2}.

A few of the built-ins, such as {\tt pie} and {\tt ksi}, provide
alternate spellings
    \footnote{If math departments around the world can get away with
       eating massive amounts of pie every year on March 14th, I think
       I can get away with this.
    }
for some of the Greek letters ($\pi$ and $\xi$,
in this case). All Greek letters whose names are two letters long are
respelled in order to avoid collisions with {\tt VerTeX}'s
automatic subscripting mechanism, as described in Section
\ref{sec:semiautomatic}; those with longer names are given
abbreviated respellings simply to allow for faster typing. The
built-ins and the {\tt TeX} that they translate to are shown in Table 2.
\begin{table}[h]
  \begin{center}
  \begin{tabular}{|c|c||c|c|} \hline
{\tt ccc} & {\tt \textbackslash{}cdots} & {\tt ddd} & {\tt \textbackslash{}ldots} \\\hline
{\tt vvv} & {\tt \textbackslash{}vdots} & {\tt equ} & {\tt =} \\\hline
{\tt div} & {\tt |} & {\tt plus} & {\tt +} \\\hline
{\tt minus} & {\tt -} & {\tt to} & {\tt \textbackslash{}rightarrow} \\\hline
{\tt gets} & {\tt \textbackslash{}leftarrow} & {\tt in} & {\tt \textbackslash{}in} \\\hline
{\tt ltn} & {\tt <} & {\tt gtn} & {\tt >} \\\hline
{\tt del} & {\tt \textbackslash{}partial} & {\tt mod} & {\tt ~\textbackslash{}mathrm\{mod\}~} \\\hline
{\tt inv} & {\tt \textasciicircum{}\{-1\}} & {\tt squ} & {\tt \textasciicircum{}2} \\\hline
{\tt cubed} & {\tt \textasciicircum{}3} & {\tt star} & {\tt \textasciicircum{}*} \\\hline
{\tt mult} & {\tt \textasciicircum{}\textbackslash{}times} & {\tt empty} & {\tt \textbackslash{}varnothing} \\\hline
{\tt plmi} & {\tt \textbackslash{}pm} & {\tt mipl} & {\tt \textbackslash{}mp} \\\hline
{\tt pie} & {\tt \textbackslash{}pi} & {\tt Pie} & {\tt \textbackslash{}Pi} \\\hline
{\tt ksi} & {\tt \textbackslash{}xi} & {\tt Ksi} & {\tt \textbackslash{}Xi} \\\hline
{\tt new} & {\tt \textbackslash{}nu} & {\tt mew} & {\tt \textbackslash{}mu} \\\hline
{\tt eps} & {\tt \textbackslash{}varepsilon} & {\tt vphi} & {\tt \textbackslash{}varphi} \\\hline
{\tt vtheta} & {\tt \textbackslash{}vartheta} & {\tt vpi} & {\tt \textbackslash{}varpi} \\\hline
{\tt sig} & {\tt \textbackslash{}sigma} & {\tt Sig} & {\tt \textbackslash{}Sigma} \\\hline
{\tt lam} & {\tt \textbackslash{}lambda} & {\tt Lam} & {\tt \textbackslash{}Lambda} \\\hline
{\tt gam} & {\tt \textbackslash{}gamma} & {\tt Gam} & {\tt \textbackslash{}Gamma} \\\hline
{\tt alp} & {\tt \textbackslash{}alpha} & & \\\hline
  \end{tabular}
  \end{center}
  \label{tab:builtins}
  \caption{built-in keywords}
\end{table}

\subsection{bracket words}

In {\tt TeX} there are many constructions in which a keyword
takes arguments surrounded by braces \{\:\}. For example,
\begin{center}
  \begin{verbatim}
    \frac{\pi}{4}
  \end{verbatim}
\end{center}
yields
\[\frac{\pi}{4}\]

In {\tt VerTeX} the same construction is achieved by
\begin{center}
  \begin{verbatim}
    frac pie over 4;
  \end{verbatim}
\end{center}

In this example, {\tt frac}, {\tt over}, and the final {\tt ;} serve
as \emph{bracket words}.
The idea of course is
that if a certain construction takes arguments, then
the arguments are to be surrounded by the appropriate bracket words.
For the most part, the final bracket word will be a semicolon.

The list of all such constructions in {\tt VerTeX} is given in
Table 3.
Here, $A'$, $B'$, and $C'$ stand for the {\tt TeX} to which the {\tt VerTeX}
$A$, $B$, and $C$ would be translated.
\begin{table}
  \begin{center}
  \begin{tabular}{|c|c|c|} \hline
    {\tt VerTeX}: & is translated into the {\tt TeX}: & mnemonic / idea
    \\\hline
    $A$ {\tt of} $B$ {\tt ;} &
        $A'$ {\tt (} $B'$ {\tt )} &
        function evaluation ``$f$ of $x$'' \\\hline
    {\tt qnt} $A$ {\tt ;} &
        {\tt \textbackslash{}left(} $A'$ {\tt \textbackslash{}right)} &
        ``quantity'' \\\hline
    {\tt bqnt} $A$ {\tt ;} &
        {\tt \textbackslash{}left[} $A'$ {\tt \textbackslash{}right]} &
        ``bracket quantity'' \\\hline
    {\tt set} $A$ {\tt ;} &
        {\tt \textbackslash{}\{ $A'$ \textbackslash{}\} } &
        ``set $A$'' \\\hline
    {\tt bset $A$ ;} &
        {\tt \textbackslash{}left\textbackslash{}lbrace A'
             \textbackslash{}right\textbackslash{}rbrace} &
        ``big set $A$'' \\\hline
    {\tt abs $A$ ;} &
        {\tt \textbackslash{}left| $A'$ \textbackslash{}right|} &
        ``absolute value'' \\\hline
    {\tt seq $A$ ;} &
        {\tt \textbackslash{}left\textbackslash{}langle $A'$
             \textbackslash{}right\textbackslash{}rangle} &
        ``sequence'' \\\hline
    {\tt sup $A$ ;} &
        {\tt \textasciicircum\{ $A'$ \}} &
        ``superscript'' \\\hline
    {\tt supp $A$ ;} &
        {\tt \textasciicircum\{( $A'$ )\}} &
        ``superscipt in parentheses'' \\\hline
    {\tt sub $A$ ;} &
        {\tt \_\{ $A'$ \}} &
        ``subscript'' \\\hline
    {\tt eth $A$ ;} &
        {\tt \textasciicircum\{\textbackslash{}mathrm\{$A'$\}\}} &
        ordinals, $1^{\mathrm{s t}}$, $2^{\mathrm{n d}}$, $3^{\mathrm{r d}}$, $4^{\mathrm{t
h}}$, ... \\\hline
    {\tt pmod $A$ ;} &
        {\tt \textbackslash{}left(\textbackslash{}mathrm\{mod\}
             \textbackslash{}, $A'$ \textbackslash{}right)} &
        ``parenthesis mod'' \\\hline
    {\tt sqrt $A$ ;} &
        {\tt \textbackslash{}sqrt\{ $A'$ \}} &
        square root \\\hline
    {\tt bar $A$ ;} &
        {\tt \textbackslash{}bar\{ $A'$ \}} &
        over bar \\\hline
    {\tt tilde $A$ ;} &
        {\tt \textbackslash{}tilde\{ $A'$ \}} &
        tilde overscript \\\hline
    {\tt hat $A$ ;} &
        {\tt \textbackslash{}hat\{ $A'$ \}} &
        hat overscript \\\hline
    {\tt words $A$ ;} &
        {\tt \textbackslash{}:\textbackslash{}mbox\{ $A'$
             \}\textbackslash{}:} &
        plain text in math mode \\\hline
    {\tt frac $A$ over $B$ ;} &
        {\tt \textbackslash{}frac\{ $A'$ \}\{ $B'$ \}} &
        quotient \\\hline
    {\tt root $A$ base $B$ ;} &
        {\tt \textbackslash{}sqrt[$A'$]\{$B'$\}} &
        general root \\\hline
    {\tt legen $A$ over $B$ ;} &
        {\tt \textbackslash{}left(\textbackslash{}frac\{$A'$\}
             \{$B'$\}\textbackslash{}right)} &
        Legendre symbol \\\hline
    {\tt binom $A$ choose $B$ ;} &
        {\tt \textbackslash{}binom\{$A'$\}\{$B'$\}} &
        binomial coefficient \\\hline
    {\tt map $A$ from $B$ to $C$ ;} &
        {\tt $A'$ : $B'$ \textbackslash{}rightarrow $C'$} &
        map \\\hline
    {\tt sum over $A$ ;} &
        {\tt \textbackslash{}sum\_\{$A'$\}} &
        sum \\\hline
    {\tt sum over $A$ from $B$ to $C$ ;} &
        {\tt \textbackslash{}sum\_\{$A'$ $=$ $B'$\}\textasciicircum\{$C'$\}} &
        sum \\\hline
    {\tt prod over $A$ ;} &
        {\tt \textbackslash{}prod\_\{$A'$\}} &
        product \\\hline
    {\tt prod over $A$ from $B$ to $C$ ;} &
        {\tt \textbackslash{}prod\_\{$A'$ $=$ $B'$\}\textasciicircum\{$C'$\}} &
        product \\\hline
    {\tt union over $A$ ;} &
        {\tt \textbackslash{}bigcup\_\{$A'$\}} &
        union \\\hline
    {\tt union over $A$ from $B$ to $C$ ;} &
        {\tt \textbackslash{}bigcup\_\{$A'$ $=$ $B'$\}\textasciicircum\{$C'$\}} &
        union \\\hline
    {\tt inters over $A$ ;} &
        {\tt \textbackslash{}bigcap\_\{$A'$\}} &
        intersection \\\hline
    {\tt inters over $A$ from $B$ to $C$ ;} &
        {\tt \textbackslash{}bigcap\_\{$A'$ $=$ $B'$\}\textasciicircum\{$C'$\}} &
        intersection \\\hline
    {\tt limit over $A$ ;} &
        {\tt \textbackslash{}lim\_\{$A'$\}} &
        limit \\\hline
  \end{tabular}
  \end{center}
  \caption{Bracket word constructions}
\end{table}

%\medskip
%\noindent {\bf Note:} 
%Besides {\tt sum} in the last two lines of Table 3, other range
%operator keywords {\tt prod}, {\tt limit}, {\tt union}, and
%{\tt inters} can also be used.

\subsection{font prefixes}

Technically font prefixes are not ``keywords'' in and of
themselves. They are two- or three-character prefixes which, when
followed by a letter of the alphabet, produce that letter in the
appropriate font. The prefixes and the fonts that they correspond to
are listed in Table 4.
\begin{table}[h]
  \begin{center}
  \begin{tabular}{|c|c|}
    \hline
    prefix & font \\\hline
    {\tt fr} & {\tt mathfrak} \\
    {\tt sf} & {\tt mathsf} \\
    {\tt bf} & {\tt mathbf} \\
    {\tt bb} & {\tt mathbb} \\
    {\tt cal} & {\tt mathcal} \\
    {\tt scr} & {\tt mathscr} \\\hline
  \end{tabular}
  \end{center}
  \label{tab:fontprefixes}
  \caption{Font prefixes}
\end{table}

Thus, for example, instead of
\begin{center}
  \begin{verbatim}
    \mathfrak{p} \quad \mathsf{M} \quad \mathbf{v} \quad
    \mathbb{Q} \quad \mathcal{O} \quad \mathscr{B}
  \end{verbatim}
\end{center}
you may type
\begin{center}
  \begin{verbatim}
    frp quad sfM quad bfv quad bbQ quad calO quad scrB
  \end{verbatim}
\end{center}
to get
\[\mathfrak{p}\quad\mathsf{M}\quad\mathbf{v}\quad\mathbb{Q}\quad\mathcal{O}\quad\mathscr{B}.\]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Semiautomatic subscripts and superscripts}
\label{sec:semiautomatic}

In mathematics, subscripted variables are the coin of the realm, and
therefore it ought to be easy to type them. {\tt VerTeX} makes it fast
and easy to get subscripts and superscripts. Table 5  shows a few examples.
\begin{table}[h]
  \begin{center}
  \begin{tabular}{|c|c|c|}
    \hline
    {\tt TeX} & {\tt VerTeX} & output \\ \hline
    {\tt a\_1, a\_2, \textbackslash{}ldots, a\_\{n+1\}} &
      {\tt a1, a2, ddd, an+1} &
      $a_{1},a_{2},\ldots,a_{n+1}$ \\\hline
    {\tt x\_\{i\_1\}, x\_\{i\_2\}, \textbackslash{}ldots, x\_\{i\_m\}} &
      {\tt xivv1, xivv2, ddd, xivvm} &
      $x_{i_{1}},x_{i_{2}},\ldots,x_{i_{m}}$ \\\hline
    {\tt a\_\{i j\}\textasciicircum{}2} &
      {\tt aijuu2} &
      $a_{i j}^{2}$ \\\hline
  \end{tabular}
  \end{center}
  \label{tab:subscriptexamples}
  \caption{Semiautomatic subscript and superscript examples}
\end{table}

The semiautomatic subscripting and superscripting (henceforth SSS)
mechanism of {\tt VerTeX} is very handy, and, as the examples in Table
5 show, makes it much easier to type certain
common kinds of subscripts and superscripts.

While many subscript and superscript combinations can be achieved
through SSS, some things are not possible. In such cases, one can use
the {\tt sub} and {\tt sup} keywords as in Table 3, or can even
fall back on standard TeX syntax.

The complete description of the SSS process is a bit complex, but for
most common purposes it is quite simple. Therefore before we give a
detailed specification of the process, we consider the main ideas.

First we need some terminology. In a figure such as $B_s^t$
we of course refer to $s$ and $t$ as subscript and superscript.
Let us refer to $B$ as the ``base.''

In most cases, SSS is simple. {\tt VerTeX} will take a word
$\omega$ and split it as $\omega=\beta\sigma$, where $\beta$ is the
longest initial segment of $\omega$ that matches as a letter name, and $\sigma$
is everything that remains. Then $\beta$ will be made the base, and $\sigma$
will be the subscript.

Examples:
\begin{center}
  \begin{tabular}{rcl}
    {\tt pi} & $\rightarrow$ & $p_{i}$ \\
    {\tt alphan} & $\rightarrow$ & $\alpha_{n}$ \\
    {\tt bbZm} & $\rightarrow$ & $\mathbb{Z}_{m}$ \\
    {\tt cn+1} & $\rightarrow$ & $c_{n+1}$ \\
    {\tt ai,j} & $\rightarrow$ & $a_{i,j}$ \\
    {\tt zeta} & $\rightarrow$ & $\zeta$ \\
  \end{tabular}
\end{center}

There are several things to note about these examples:
\begin{enumerate}
\item It was so that {\tt pi} could be available for automatic subscripting
  that we gave the Greek letter $\pi$ the (admittedly somewhat silly)
  spelling {\tt pie}. Writing $p_{i}$ is a daily occurrence for anyone
  who works with prime numbers, and this includes a lot of
  mathematicians.

\item Letters with extended names, like {\tt alpha}, and letters with
a font prefix in front of them, like {\tt bbZ}, will indeed be counted
as initial letters.

\item Commas, as well as plus and minus signs, are considered part of
  the word. Read further details about this below.

  \medskip 
  {\bf NB:}
  this is one way in which {\tt VerTeX} is \emph{not transparent} to
  ordinary TeX.

\item What happened with {\tt zeta}? Perhaps we were hoping for
  $z_{\eta}$, but of course {\tt VerTeX} instead matched the entire word
  {\tt zeta} as the base. There is a way to get around this, which we
  discuss below.
  Preview: You may type {\tt zvveta} in order to get $z_{\eta}$.

\end{enumerate}

\noindent {\bf The details.}

To the {\tt VerTeX} parser, a ``word'' consists of alphanumeric
characters, as well as commas and the plus and minus symbols. It must
begin with an alphabetical character
\footnote{In other words a ``letter,'' but this is one of the letters
  in the character class {\tt [A-Za-z]}, and is not to be confused
  with all things considered letters in {\tt VerTeX}, which includes,
  for example, Greek letters, and letters with font prefixes.}
For those familiar with regular expressions, this means that words are
built on the character class
\begin{center}
  {\tt [A-Za-z0-9+-,]}
\end{center}
The last three symbols are included in the character class because
they are common in subscripts. {\bf However,} this means that if you
do not want to accidentally trigger a subscript, you need to put
whitespace on at least one side of these characters. This is one way
in which {\tt VerTeX} is {\bf not transparent} to ordinary TeX.

\medskip
Now suppose that $\omega$ is the next word that {\tt VerTeX} has to
process. If $\omega$ fails to match as any kind of keyword -- bsme,
built-in, bracket word, or user-defined keyword (see Section \ref{sec:macros})
-- then $\omega$ is submitted to the SSS process.

{\tt VerTeX} first matches the longest possible letter name at the
beginning of $\omega$, as we discussed above.
%. For example, if $T =$ {\tt alphan} this will be {\tt
%  alpha}; if $T =$ {\tt bx} this will be {\tt b}. Letters with font
%prefixes count as letters here, so if $T =$ {\tt bbZn} then {\tt bbZ}
%will match as the initial letter.
Let the word $\omega$ consist of initial letter $\beta$ followed by
remainder $\sigma$, that is, $\omega=\beta\sigma$. Then $\beta$ will be the
base, and $\sigma$ will give one or more subscripts and/or superscripts.

In the simplest case, $\sigma$ simply represents a subscript. It is
possible however to switch between subscripts and superscripts using
the special character sequences {\tt vv}, {\tt uu}, and {\tt UU}.

A few examples illustrate all of the ways to use these control sequences:
\begin{center}
  \begin{tabular}{rcl}
    {\tt auur} & $\rightarrow$ & $a^{r}$ \\
    {\tt aiuur} & $\rightarrow$ & $a_{i}^{r}$ \\
    {\tt auurvvk} & $\rightarrow$ & $a^{r_{k}}$ \\
    {\tt aivvj} & $\rightarrow$ & $a_{i_{j}}$ \\
    {\tt aivvjuur} & $\rightarrow$ & $a_{i_{j}^{r}}$ \\
    {\tt aivvjUUr} & $\rightarrow$ & $a_{i_{j}}^{r}$ \\
    {\tt zvveta} & $\rightarrow$ & $z_{\eta}$ \\
  \end{tabular}
\end{center}

Sequence {\tt vv} opens a deeper subscript. In TeX it is as though you
typed {\tt \_\{}.

Sequence {\tt uu} closes a subscript \emph{and} opens a
superscript. In TeX it is as though you typed {\tt \}\textasciicircum{}\{}.

Seuqnece {\tt UU} closes \emph{two} subscripts and opens a
superscript. In TeX it is as though you typed {\tt \}\}\textasciicircum{}\{}.

An exception is that if {\tt vv} is used at the very beginning of
$\sigma$, it merely keeps you at the first subscript level. Thus,
{\tt zvveta} provides a way to produce $z_{\eta}$, while {\tt zeta} simply
gives $\zeta$.

%examples:
%
% ai, alphai (matching longest possible initial letter)
% frPi (initial letter can be in any font)
% chieta (letters in subscript will be passed through VerTeX filter)
% 

%\[ etai+j quad etai + j quad xn-1 quad xn - 1 quad ai,j quad ai, j \]
%\[ bbZ \]
%\[ bbQp \]
%\[ AbbQ \]
%\[ xeta quad zeta quad zvveta quad z sub eta; quad beta quad bvveta \]
%\[ aivvj quad aivvjuur quad aiuur quad aivvjUUr \]
%\[ pi quad pie quad pvvie quad \\pie quad p i e \]
%
%\begin{enumerate}
%  \item Let $T$ be the next word that {\tt VerTeX} has to consider. If
%    $T$ fails to match as any kind of keyword (bsme, built-in, bracket
%    word, or user-defined keyword (see Section \ref{??})), then, and
%    only then, we submit $T$ to the SSS process.
%
%  \item SSS process on word $T$:
%    \begin{enumerate}
%      \item Match the longest letter name at the beginning of $T$,
%      and let $T = a r$, where $a$ is this letter name, and $r$ is the
%      remainder of $T$. Here, a letter name is any of the following:
%      \begin{itemize}
%	\item ...
%      \end{itemize}
%
%      \item Split $r$ into a sequence of tokens, $r = r1 r2 ccc rm$,
%      where tokens may be any of the following:
%      \begin{itemize}
%	\item ...
%        \item The special tokens {\tt uu}, {\tt UU}, and {\tt vv}.
%      \end{itemize}
%
%      \item translate those tokens that need it, e.g. Greek letter
%             names; handle uu and vv appropriately
%    \end{enumerate}
%\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Macros on the fly}
\label{sec:macros}

At any point in a {\tt VerTeX} document, you may write a special
comment line that begins with {\tt \%\%vtx\%\%}, signaling that you
are going to give a macro definition.

If $k$ is the keyword that you want to define, and if $t$ is the text
that it should expand to, then the syntax is as follows:
\begin{center}
  {\tt \%\%vtx\%\% $k$ \%\% $t$ \%\%;}
\end{center}

If you want the keyword to take arguments, simply name the arguments
in the expansion text $t$ using {\tt \#1}, {\tt \#2}, {\tt \#3}, and
so on. When used, the arguments to the keyword should be separated by
semicolons.

Examples:

After the line
\begin{center}
  {\tt \%\%vtx\%\% NLK \%\% N sub L | K; \%\%;},
\end{center}
{\tt NLK} expands to {\tt N sub L | K;}, producing $N_{L|K}$.

After the line
\begin{center}
  {\tt \%\%vtx\%\% atoq \%\% a sub \#1; sup q sub \#1; of \#2 ;; \%\%;},
\end{center}
{\tt atoq n ; x ;} expands to {\tt a sub n; sup q sub n; of x;;},
producing $a_{n}^{q_{n}(x)}$.

\section{Transparency to ordinary TeX}
In general, it is hoped that 99\% of the time ordinary {\tt TeX} will
pass through VerTeX unaltered, so that you can use as many or as few of
the VerTeX features as you wish.

The main known lack of transparency is that discussed in
Section~\ref{sec:semiautomatic} on
the SSS mechanism. Namely, you must put whitespace on at least one
side of any comma, plus, or minus symbol in a math environment in
order to prevent unwanted automatic subscripting.

The other main issue is that the {\tt VerTeX} parser needs to look
through your input document in search of math modes, and each new kind
of math environment needs to programmed in. While most of the common
environments, such as {\tt equation} and AMS {\tt align}, are supported,
some environments, such as the {\tt commdiag} environment in the
{\tt xypic} package, are not yet supported.

Apart from these points, the author has not yet discovered any other
issues while making regular use of the system for several years.
Of course, bug reports are welcome.\footnote{
Visit \url{http://skieffer.info}.
}

\medskip
As a ``safety net,'' any word whatsoever may be prefixed with a double
backslash \textbackslash{}\textbackslash{}, in order to allow that
word to pass through {\tt VerTeX} unaltered. To be precise, if there is
any remainder $w$ to the word, this, without the two backslashes, is
what will pass through. If just two backslashes alone are typed, they
will pass though unaltered (which is useful in TeX table
environments).
Meanwhile, any word beginning with a single backslash is passed
through {\tt VerTeX} completely unaltered, i.e. with the leading
backslash still intact.

In summary:
\begin{center}
  \begin{tabular}{lcl}
    \textbackslash\textbackslash$w$ & $\mapsto$ & $w$ \\
    \textbackslash\textbackslash & $\mapsto$ &
    \textbackslash\textbackslash \\
    \textbackslash$w$ & $\mapsto$ & \textbackslash$w$
  \end{tabular}
\end{center}
where $w$ is a word at least one character long.

%\section{{\tt VerTeX} by example}
%The best way to learn {\tt VerTeX} is the same as the best way to learn
%{\tt TeX}: by example. We give here the source text for an {\tt vtx}
%file that uses most of the tricks we've covered in this manual,
%followed by the resulting output.
%
%{\tt UNDER CONSTRUCTION}

\end{document}
